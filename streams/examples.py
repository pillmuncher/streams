#!/usr/bin/env python3
# -*- coding: utf-8

from .combinators import const, function, unstar
from .streams import Stream


def test_stream():
    s = Stream('abcdefgh')
    it1, it2, it3 = s.tee(3)
    it1 += it1
    it2 = it2.window(3).map(''.join)
    it3 = it3.accumulate()
    it1.subscribe(on_send=print, on_close=lambda: print('it1 done'))
    it2.subscribe(on_send=print, on_close=lambda: print('it2 done'))
    it3.subscribe(on_send=print, on_close=lambda: print('it3 done'))
    for i in range(10):
        print('pull:', i)
        it1.pull(1)
        it2.pull(1)
        it3.pull(1)


def test_async():
    s = Stream.Async('abcdefgh')
    it3, it2, it1 = s.tee(3).take(100)
    it1 += it1
    it2 = it2.window(3).map(''.join)
    it3 = it3.accumulate()
    it1.subscribe(on_send=print, on_close=lambda: print('it1 done'))
    it2.subscribe(on_send=print, on_close=lambda: print('it2 done'))
    it3.subscribe(on_send=print, on_close=lambda: print('it3 done'))
    for i in range(20):
        print('pull:', i)
        it1.pull(1)
        it2.pull(1)
        it3.pull(1)


def test_interval_class():
    s = Stream.Interval(.5, 'abcdefgh')
    it3, it2, it1 = s.tee(3)
    it1 += it1
    it2 = it2.window(3).map(''.join)
    it3 = it3.accumulate()
    it1.subscribe(on_send=print, on_close=lambda: print('it1 done'))
    it2.subscribe(on_send=print, on_close=lambda: print('it2 done'))
    it3.subscribe(on_send=print, on_close=lambda: print('it3 done'))
    for i in range(20):
        print('pull:', i)
        it1.pull(1)
        it2.pull(1)
        it3.pull(1)


def test_interval_method():
    s = Stream('abcdefgh').interval(.5)
    it3, it2, it1 = s.tee(3)
    it1 += it1
    it2 = it2.window(3).map(''.join)
    it3 = it3.accumulate()
    it1.subscribe(on_send=print, on_close=lambda: print('it1 done'))
    it2.subscribe(on_send=print, on_close=lambda: print('it2 done'))
    it3.subscribe(on_send=print, on_close=lambda: print('it3 done'))
    for i in range(20):
        print('pull:', i)
        it1.pull(1)
        it2.pull(1)
        it3.pull(1)


def test_throttle_method():
    s = Stream.count().interval(.002).throttle(.007)
    it3, it2, it1 = s.tee(3)
    it1 += it1.take(30)
    it2 = it2.window(3).map(list)
    it3 = it3.accumulate()
    it1.subscribe(on_send=print, on_close=lambda: print('it1 done'))
    it2.subscribe(on_send=print, on_close=lambda: print('it2 done'))
    it3.subscribe(on_send=print, on_close=lambda: print('it3 done'))
    for i in range(20):
        print('pull:', i)
        it1.pull(1)
        it2.pull(1)
        it3.pull(1)


def test_signal():
    from threading import Thread

    s = Stream.Signal()
    it3, it2, it1 = s.interval(.5).tee(3).take(100)
    it1 += it1
    it2 = it2.window(3).map(''.join)
    it3 = it3.accumulate()
    it1.subscribe(on_send=print, on_close=lambda: print('it1 done'))
    it2.subscribe(on_send=print, on_close=lambda: print('it2 done'))
    it3.subscribe(on_send=print, on_close=lambda: print('it3 done'))

    def generate():
        for c in 'abcdefgh':
            s.send(c)
        s.close()

    Thread(target=generate, daemon=True).start()

    for i in range(20):
        print('pull:', i)
        it1.pull(1)
        it2.pull(1)
        it3.pull(1)


@function
def foo(x, y=1, *, z):
    print(x, y, z)


@function
def bar(*u, v, **w):
    print(u)
    print(v)
    print(w)


def test_curry():
    f = foo
    f = f()
    f = f(z=3)
    f = f(y=2)
    f = f(x=1)
    b = bar
    b = b(1, 2, 3)
    b = b(4, 5, 6)
    b = b(x=8, y=9)
    b = b(v=7)


@function
def zig(x, y):
    return x + y


@function
def zag(z):
    return z * 2


def test_composition():
    z = zag * zig
    print(z.__signature__)
    print(str(z))
    print(z(4))
    print('!!!', z(4)(5))


def test_empty():
    s = Stream.empty()
    for each in s:
        print('never here')
    else:
        print('but here')


def test_take_last():
    it1, it2 = Stream.count().tee()
    it2 = it2.map(const(100))
    it3 = (it1 + it2).take(10).take(-3)
    it3.subscribe(on_send=print, on_close=lambda: print('it3 done'))
    it3.pull()


def test_slice():
    it1, it2 = Stream.count().tee()
    it2 = it2.map(const(100))
    it3 = (it1 + it2).slice(start=1, step=2).take(10)
    it3.subscribe(on_send=print, on_close=lambda: print('it3 done'))
    it3.pull()


def test_count_running():

    from functools import partial
    from operator import eq
    from random import randint

    def get_running_values(i, n, c):
        return n, c, '{0:.2%}'.format(c / i)

    it1, it2 = Stream.call_forever(randint, 0, 9).tee()
    it2 = it2.map(partial(eq, 2), int).accumulate()
    it3 = (
        Stream
        .zip(Stream.count(start=1), it1, it2)
        .starmap(get_running_values)
    )
    it3.subscribe(on_send=unstar(print))
    it3.pull(20)
    it3.pull(10)


if __name__ == '__main__':
    #test_stream()
    #test_async()
    #test_interval_class()
    #test_interval_method()
    #test_throttle_method()
    test_signal()
    #test_curry()
    #test_composition()
    #test_empty()
    #test_take_last()
    #test_slice()
    #test_count_running()
