#!/usr/bin/env python3
# -*- coding: utf-8

import queue

from enum import Enum
from functools import wraps, partial, reduce
from itertools import chain, count, cycle, islice, groupby, repeat, tee
from itertools import zip_longest, accumulate
from operator import add, mul, or_
from queue import Queue
from threading import Thread, Event
from time import sleep

from .combinators import noop, const, compose, unstar


def _compose(f, *gs):
    def composed(*a, **k):
        x = f(*a, **k)
        for g in gs:
            x = g(x)
        return x
    return composed


class Message(Enum):

    Event = 1
    Error = 2
    Close = 3

    def __call__(self, msg=None):
        return self, msg


class Observer:

    def on_send(self, event):
        pass

    def on_throw(self, error):
        pass

    def on_close(self):
        pass

    def send(self, event):
        self.on_send(event)

    def throw(self, error):
        self.on_throw(error)

    def close(self):
        self.on_close()


class Delegate(Observer):
    def __init__(self, on_send=None, on_throw=None, on_close=None):
        self.on_send = on_send or noop
        self.on_throw = on_throw or noop
        self.on_close = on_close or noop


class Observable:

    def __new__(cls, *a, **k):
        new = super().__new__(cls)
        new.observers = set()
        return new

    def subscribe(self, *obs, on_send=None, on_throw=None, on_close=None):
        def unsubscribe():
            self.observers -= observers
            observers.clear()
        observers = set(obs)
        if on_send or on_throw or on_close:
            observers.add(Delegate(
                on_send=on_send,
                on_throw=on_throw,
                on_close=on_close,
            ))
        self.observers |= observers
        return unsubscribe

    def _send(self, event):
        for each in self.observers:
            each.send(event)

    def _throw(self, error):
        for each in self.observers:
            each.throw(error)
        self.observers.clear()

    def _close(self):
        for each in self.observers:
            each.close()
        self.observers.clear()


class Subject(Observable, Observer):

    def on_send(self, event):
        self._send(event)

    def on_throw(self, error):
        self._throw(error)

    def on_close(self):
        self._close()


class Iterator(Observable):

    def __init__(self, iterable):
        self.iterable = iter(iterable)

    def __iter__(self):
        try:
            for event in self.iterable:
                self._send(event)
                yield event
        except Exception as error:
            self._throw(error)
            raise
        else:
            self._close()

    def pull(self, n=None):
        if n is None:
            for event in self:
                pass
        else:
            for event in Stream(self).take(n):
                pass


def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


def roundrobin(*iterables):
    "roundrobin('ABC', 'D', 'EF') --> A D E B F C"
    # Recipe credited to George Sakkis
    pending = len(iterables)
    nexts = cycle(iter(it).__next__ for it in iterables)
    while pending:
        try:
            for next in nexts:
                yield next()
        except StopIteration:
            pending -= 1
            nexts = cycle(islice(nexts, pending))


def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def window(iterable, size):
    return unstar(zip)(
        islice(each, i, None) for i, each in enumerate(tee(iterable, size))
    )


def grouped(iterable, key=None):
    for _, group in groupby(iterable, key):
        yield list(group)


def take_last(stream, n):
    window = ()
    for window in stream.window(n):
        pass
    else:
        yield from window


def split(pred, iterable):
    acc = []
    for item in iterable:
        acc.append(item)
        if pred(item):
            yield acc
            acc = []
    if acc:
        yield acc


def join_streams(*pending, scheduler):
    pending = list(pending)
    while pending:
        stream = scheduler(pending)
        for event in stream:
            yield event
            break
        else:
            pending.remove(stream)


def make_op(op):
    @wraps(op)
    def op_method(self, other):
        return Stream(map(op, self, other))
    return op_method


class StreamMeta(type(Iterator)):

    @staticmethod
    def empty():
        return Stream(())

    @staticmethod
    def once(event):
        return Stream([event])

    @staticmethod
    def repeat(event):
        return Stream(repeat(event))

    @staticmethod
    def call_forever(func, *args, **kwargs):
        return Stream(iter(lambda: func(*args, **kwargs), Message.Close))

    @staticmethod
    def cycle(events):
        return Stream(cycle(events))

    @staticmethod
    def count(start=0):
        return Stream(count(start))

    @staticmethod
    def lift(func):
        return wraps(func)(lambda stream: Stream.map(stream, func))


class Stream(Iterator, metaclass=StreamMeta):

    def bind(self, mfunc):
        return mfunc(self)

    def start_async(self, func, *args, **kwargs):
        run = partial(func, self, *args, **kwargs)
        Thread(target=run, daemon=True).start()
        return self

    def const(self, c):
        return self.map(const(c))

    def zip(self, stream, *streams):
        return Stream(zip(self, stream, *streams))

    def map(self, func, *funcs):
        project = compose(func, *funcs)
        return Stream(project(each) for each in self)

    def starmap(self, func, *funcs):
        project = _compose(func, *funcs)
        return Stream(project(*each) for each in self)

    def filter(self, pred):
        return Stream(v for v in self if pred(v))

    def filterfalse(self, pred):
        return Stream(v for v in self if not pred(v))

    def filter_by_index(self, pred):
        return Stream(v for i, v in enumerate(self) if pred(i))

    def filterfalse_by_index(self, pred):
        return Stream(v for i, v in enumerate(self) if not pred(i))

    def enumerate(self, start=0):
        return Stream(enumerate(self, start=start))

    def accumulate(self, func=None):
        return Stream(accumulate(self, func=func))

    def pairwise(self):
        return Stream(pairwise(self))

    def window(self, size):
        return Stream(window(self, size))

    def split(self, pred):
        return Stream(split(pred, self))

    def chain(self, *others):
        return Stream(chain(self, *others))

    def take(self, n):
        if n > 0:
            return Stream(islice(self, n))
        elif n < 0:
            return Stream(take_last(self, -n))
        else:
            return Stream.empty()

    def slice(self, *, start=0, stop=None, step=1):
        return Stream(islice(self, start, stop, step))

    def tee(self, n=2):
        return Stream(tee(self, n)).map(Stream)

    def reduce(self, func, initializer=None):
        return Stream.once(reduce(self, initializer=initializer))

    def groupby(self, key=None):
        return Stream(grouped(self, key=key))

    def group(self, size, fillvalue=None):
        return Stream(grouper(self, size, fillvalue))

    def flatten(self):
        return Stream(chain.from_iterable(self))

    def join(self, *streams, scheduler=None):
        if scheduler is None:
            return Stream(roundrobin(*streams))
        else:
            return Stream(join_streams(self, *streams, scheduler=scheduler))

    def combine(self, *streams, func=None):
        if func is None:
            return Stream.zip(self, *streams)
        else:
            return Stream(map(func, self, *streams))

    def interval(self, t):
        return IntervalStream(t, self)

    def throttle(self, t):
        return ThrottledStream(t, self)

    __add__ = make_op(add)
    __mul__ = make_op(mul)
    __or__ = make_op(or_)

    #__getitem__ = make_op()
    #__call__ = make_op()
    #__neg__ = make_op()
    #__pos__ = make_op()
    #__invert__ = make_op()
    #__add__ = make_op()
    #__radd__ = make_op()
    #__sub__ = make_op()
    #__rsub__ = make_op()
    #__mul__ = make_op()
    #__rmul__ = make_op()
    #__truediv__ = make_op()
    #__rtruediv__ = make_op()
    #__floordiv__ = make_op()
    #__rfloordiv__ = make_op()
    #__mod__ = make_op()
    #__rmod__ = make_op()
    #__pow__ = make_op()
    #__rpow__ = make_op()
    #__lshift__ = make_op()
    #__rlshift__ = make_op()
    #__rshift__ = make_op()
    #__rrshift__ = make_op()
    #__and__ = make_op()
    #__rand__ = make_op()
    #__xor__ = make_op()
    #__rxor__ = make_op()
    #__or__ = make_op()
    #__ror__ = make_op()


class _Async(Stream):

    _Queue = Queue

    def __init__(self):
        self.queue = self._Queue()
        super().__init__(self._pull_all())

    def _push(self, event):
        self.queue.put(event)

    def _pull(self):
        return self.queue.get()

    def _pull_all(self):
        for msg_type, msg in Stream.call_forever(self._pull):
            if msg_type == Message.Event:
                yield msg
            elif msg_type == Message.Error:
                raise msg
            elif msg_type == Message.Close:
                return


class Origin(_Async):

    def send(self, event):
        self._push(Message.Event(event))

    def throw(self, error):
        self._push(Message.Error(error))

    def close(self):
        self._push(Message.Close())


class AsyncStream(Origin):

    def __init__(self, iterable=None):
        if iterable is None:
            iterable = count()
        Thread(target=self._push_all, args=[iterable], daemon=True).start()
        super().__init__()

    def _push_all(self, iterable):
        try:
            for event in iterable:
                self._push(Message.Event(event))
        except Exception as error:
            self._push(Message.Error(error))
        else:
            self._push(Message.Close())


class IntervalStream(AsyncStream):

    def __init__(self, t, iterable=None):
        self.t = t
        super().__init__(iterable)

    def _push(self, event):
        sleep(self.t)
        super()._push(event)


class ThrottledStream(AsyncStream):

    class _Queue:

        def __init__(self):
            self.value = None
            self.event = Event()

        def get(self):
            self.event.wait()
            value = self.value
            self.event.clear()
            return value

        def put(self, value):
            self.value = value
            self.event.set()

    def __init__(self, t, iterable=None):
        self.t = t
        super().__init__(iterable)

    def _pull(self):
        sleep(self.t)
        return super()._pull()


Stream.Origin = Origin
Stream.Async = AsyncStream
Stream.Interval = IntervalStream
Stream.Throttled = ThrottledStream
