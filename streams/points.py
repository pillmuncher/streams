#!/usr/bin/env python3
# -*- coding: utf-8

from random import choice
from threading import Thread

from .streams import Stream
from .combinators import first


def send_points(stream, n):
    stream.send('A0')
    for i in range(1, n):
        stream.send('{}{}'.format(choice('AB'), i))
    stream.send('A{}'.format(n))
    stream.close()


def combine_window(left_as, bs, right_as):
    return left_as[-1:] + bs + right_as[:1]


def main():
    rows = (
        Stream.Origin()
        .start_async(send_points, 100)
        .groupby(first)
        .window(size=3)
        .slice(step=2)
        .starmap(combine_window)
    )
    rows.subscribe(on_send=print)
    rows.pull()


if __name__ == '__main__':
    main()
