#!/usr/bin/env python3
# -*- coding: utf-8

from functools import wraps, update_wrapper, partial, reduce as foldl
from inspect import signature
from operator import itemgetter


first = itemgetter(0)
second = itemgetter(1)
last = itemgetter(-1)


def noop(*a, **k):
    pass


def flip(f):
    @wraps(f)
    def flipped(*a):
        return f(*a[::-1])


def identity(x):  # AKA: the I combinator
    return x


def const(x):  # AKA: the K combinator
    def k(_):
        return x
    return k


def compose(*fs):
    def composed(x):
        for f in fs:
            x = f(x)
        return x
    return composed


rcompose = flip(compose)

_sentinel = object()


def foldr(func, seq, start=_sentinel):
    if start is _sentinel:
        return foldl(flip(func), reversed(seq))
    return foldl(flip(func), reversed(seq), start)


def rfoldl(func, seq, start=_sentinel):
    if start is _sentinel:
        return foldl(flip(func), seq)
    return foldl(flip(func), seq, start)


def rfoldr(func, seq, start=_sentinel):
    if start is _sentinel:
        return foldl(func, reversed(seq))
    return foldl(func, reversed(seq), start)


def rpartial(f, *args, **kwargs):
    return partial(flip(f), *args, **kwargs)


def star(f):
    @wraps(f)
    def wrapper(*xs):
        return f(xs)
    return wrapper


def unstar(f):
    @wraps(f)
    def wrapper(xs):
        return f(*xs)
    return wrapper


def fapply(x, f):
    return f(x)


class function:

    def __init__(self, func):
        update_wrapper(self, func)
        self.func = func
        self.__name__ = func.__name__

    @property
    def __signature__(self):
        return signature(self.func)

    def __call__(self, *args, **kwargs):
        ba, sig = self._bind(args, kwargs)
        if set(sig.parameters) == set(ba.arguments):
            return self.func(*ba.args, **ba.kwargs)
        else:
            f = partial(self.func, *ba.args, **ba.kwargs)
            f.__name__ = self.func.__name__
            return function(f)

    def _bind(self, args, kwargs):
        sig = signature(self.func)
        ba = sig.bind_partial(*args, **kwargs)
        for param in sig.parameters.values():
            if param.name not in ba.arguments:
                if param.default is not param.empty:
                    ba.arguments[param.name] = param.default
                elif param.kind == param.VAR_POSITIONAL:
                    ba.arguments[param.name] = []
                elif param.kind == param.VAR_KEYWORD:
                    ba.arguments[param.name] = {}
        return ba, sig

    def bind(self, *args, **kwargs):
        ba, _ = self._bind(args, kwargs)
        f = partial(self.func, *ba.args, **ba.kwargs)
        f.__name__ = self.func.__name__
        return function(f)

    def __str__(self):
        return 'function({}{})'.format(self.__name__, self.__signature__)

    def __mul__(self, other):
        f = wraps(other)(lambda *a, **k: self(other(*a, **k)))
        f.__signature__ = signature(other)
        f.__name__ = '({} * {})'.format(other.__name__, self.__name__)
        return function(f)
